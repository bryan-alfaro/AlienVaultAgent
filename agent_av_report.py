"""
    Agente Remoto para Alienvault

Este agente se encarga de recolectar la siguiente informacion
de la base de datos de alienvault
    - Casos abiertos y Cerrados.
    - Resumen de vulnerabilidades.
    - El "top" de alarmas.
    - Casos abiertos y cerrados por nivel de casos_riesgo.
    - Casos abiertos y cerrados por categoria.
la response de la clase Report() esta en formato json.



Version: 0.1.1
Fecha actualizacion: 27/2/2018
Autor: Dante Alfaro - Bryan Alfaro
Twitter: @DanteAlfarojs
"""
import json
import argparse
import mysql.connector

parse = argparse.ArgumentParser()
parse.add_argument("-u", "--user", help="Nombre de usuario")
parse.add_argument("-p", "--password", help="password del usuario")
parse.add_argument("-ho", "--host", help="Direccion del host")
parse.add_argument("-db", "--database", help="Nombre de la base de datos")
parse.add_argument("-d1", "--date1", help="Fecha inicio")
parse.add_argument("-d2", "--date2", help="Fecha fin")
parse.add_argument(
    "-vg",
    "--vul_group",
    help="Lista de los grupos en la lista de escaner"
)
args_shell = parse.parse_args()


class Reporte():

    def __casos(self):
        date1 = args_shell.date1.split("_")
        date2 = args_shell.date2.split("_")
        sqlquery2 = """select count(b.incident_id) as
            'Casos cerrados' from incident a,
            incident_ticket b where b.status like
            '%Closed%' and b.date between '{0}' and '{1}'
            and a.id=b.incident_id;
        """.format(date1[0], date2[0])

        sqlquery1 = """select count(id) as 'Casos abiertos'
            from incident where status not like '%closed%' and date <='{0}';
        """.format(date2[0])

        connect = mysql.connector.connect(
            user=args_shell.user,
            password=args_shell.password,
            host=args_shell.host,
            database=args_shell.database,
        )
        cursor = connect.cursor()
        cursor.execute(sqlquery1)
        case_open = None
        case_close = None
        for open_ticket in cursor:
            case_open = open_ticket
        cursor.execute(sqlquery2)
        for close_ticket in cursor:
            case_close = close_ticket
        connect.close()
        return {
            'abiertos': case_open,
            'cerrados': case_close
        }

    def __vulnerabilidades(self):
        date1 = args_shell.date1.split("_")
        date2 = args_shell.date2.split("_")
        # Inicia los Query para esta funcion

        # Query para los jobs programados
        sqlquery2 = """select * from vuln_job_schedule;
        """

        # Query para los jobs ejecutados
        sqlquery3 = """select * from vuln_jobs where (
            scan_START between '{0}%' and '{1}%'
        )""".format(date1[0], date2[0])

        connect = mysql.connector.connect(
            user=args_shell.user,
            password=args_shell.password,
            host=args_shell.host,
            database=args_shell.database,
        )
        cursor = connect.cursor()

        # Obtenemos los jobs programados de la tabla...
        # vuln_job_schedule y los guardamos en jobs_schedule

        cursor.execute(sqlquery2)
        jobs_schedule = []
        for jobs in cursor:
            jobs_schedule.append(jobs[1].encode("utf-8"))

        # Alienvault antepone SCHEDULED a los jobs en la tabla...
        # vuln_jobs. Por esta razon crearemos los nombres

        for job in range(0, len(jobs_schedule)):
            jobs_schedule[job] = "{0} - {1}".format(
                'SCHEDULED',
                jobs_schedule[job]
            )

        # Guardamos id y nombre de los ultimos escaneos..
        # programados y ejecutados, en last_jobs de la tabla..
        # vuln_jobs filtrados por el mes solicitado

        cursor.execute(sqlquery3)
        last_jobs = []
        for vul_job in cursor:
            last_jobs.append(
                {
                    'id': vul_job[0],
                    'name': vul_job[1].encode("utf-8")
                }
            )

        # Se elabora una lista para agrupar los ids de los...
        # registros de vuln_jobs con su respectivo nombre...
        # Ejemplo: SCHEDULED - Servers : [1,1,2,3,5]...
        # y se guardan los diccionarios en sum_jobs_sche.

        sum_jobs_sche = {}
        for j_schedule in jobs_schedule:
            for l_jobs in last_jobs:
                if j_schedule == l_jobs['name']:
                    if j_schedule in sum_jobs_sche:
                        temp = sum_jobs_sche[j_schedule]
                        temp.append(l_jobs['id'])
                    else:
                        sum_jobs_sche[j_schedule] = ([l_jobs['id']])

        # para cada SHCEDULED name en sum_jobs_sche..
        # se sacara el id con mayor valor ya que este es el...
        # ultimo jobs realizado y se guardaran en la variable..
        #

        vul_jobs_report = []
        for sum_job_sche in sum_jobs_sche:
            most_update = max(sum_jobs_sche[sum_job_sche])
            vul_jobs_report.append(
                "{0} - {1}".format(str(most_update), sum_job_sche)
            )

        # buscamos el resultado del scan mas actual por el nombre
        # los nombres son los que estan en vul_jobs_report
        resp_fin = []
        for vul_name in vul_jobs_report:
            sqlquery1 = """select * from vuln_nessus_report_stats where
            name like '{0}'
            """.format(vul_name)
            cursor.execute(sqlquery1)
            for data in cursor:
                info = {
                    'id': data[0],
                    'name': vul_name,
                    'data': {
                        'Numhosts': data[3],
                        'vserious': data[7],
                        'vhigh': data[8],
                        'vmedia': data[9]
                    }
                }
                resp_fin.append(info)
        connect.close()
        return resp_fin

    def __top_alarma(self):
        sqlquery1 = """select * from alarm where (timestamp between '{0}' and '{1}')
        """.format(args_shell.date1, args_shell.date2)
        connect = mysql.connector.connect(
            user=args_shell.user,
            password=args_shell.password,
            host=args_shell.host,
            database=args_shell.database,
        )
        cursor = connect.cursor()
        cursor.execute(sqlquery1)
        # realiza una lista con...
        # objetos tipo diccionario con las llaves sid y status
        lista_1 = []
        lista_2_sid = []
        lista_2_plugin_sid = list()
        lista_3 = []
        for col in cursor:
            lista_1.append(
                {
                    'sid': col[6],
                    'status': col[4],
                    'plugin_id': col[5]
                }
            )
            lista_2_sid.append(col[6])
            lista_2_plugin_sid.append((col[6], col[5]))
        sid_uniq = []
        for i in lista_2_sid:
            if i not in sid_uniq:
                sid_uniq.append(i)
            else:
                pass

        sid_plugin_sid_uniq = []
        lista_2_plugin_sid = set(lista_2_plugin_sid)
        # primer paso sacar cuantas veces se produjo una alarma en determinado
        # mes,
        list_f_paso_1 = []
        for i in sid_uniq:
            cantidad = lista_2_sid.count(i)
            list_f_paso_1.append([{i: cantidad}])
        # Inicio de segundo paso determinar categoria y nombres de las alarmas
        list_f_paso_2 = []
        kin = None
        cat = None
        sub = None
        for sid in list_f_paso_1:
            ke = sid[0].keys()
            ke = list(ke)
            query2 = "select * from alarm_taxonomy where sid like '{}'".format(
                ke[0]
            )

            cursor.execute(query2)
            for taxo in cursor:
                kin = taxo[2]
                cat = taxo[3]
                sub = taxo[4]
            # Evaluamos si la categoria no esta en  taxonomy y la buscamos en
            # la tabla plugin_sid
            if kin is None or cat is None or sub is None:
                query2_5 = None
                for plguin_id in lista_2_plugin_sid:
                    if plguin_id[0] == ke[0]:
                        query2_5 = "select * from plugin_sid where sid like '{}' and plugin_id like '{}'".format(
                            ke[0],
                            plguin_id[1]
                        )
                cursor.execute(query2_5)
                for name in cursor:
                    kin = name[6]
                    cat = ""
                    sub = ""
                    list_f_paso_2.append(
                        [
                          {
                              'sid': ke[0],
                              'occurre':sid[0][ke[0]],
                              'kingdom':kin,
                              'category':cat,
                              'subcat':sub
                          }
                        ]
                    )
                    kin = None
            else:
                list_f_paso_2.append(
                   [
                       {
                           'sid': ke[0],
                           'occurre': sid[0][ke[0]],
                           'kingdom': kin,
                           'category':cat,
                           'subcat':sub
                        }
                    ]
                )
        list_f_paso_3 = []
        for info in list_f_paso_2:
            kingdom = info[0]['kingdom']
            category = info[0]['category']
            query3 = "select * from alarm_kingdoms where id like '{}'".format(
                kingdom
            )

            query4 = "select * from alarm_categories where id like '{}'".format(
                category
            )

            cursor.execute(query3)
            for kingdom in cursor:
                kingdom = kingdom[1]
            info[0]['kingdom'] = kingdom
            cursor.execute(query4)
            for category in cursor:
                category = category[1]

            info[0]['category'] = category
            list_f_paso_3.append(info)

        connect.close()

        resp_fin = list()
        for data in list_f_paso_3:
            resp_fin.append(
                {
                    'kingdom': data[0]['kingdom'].encode("utf-8"),
                    'category': data[0]['category'].encode("utf-8"),
                    'subcat': data[0]['subcat'].encode("utf-8"),
                    'occurre': data[0]['occurre']
                }
            )
        return resp_fin

    def __casos_riesgo(self):
        date1 = args_shell.date1.split("_")
        date2 = args_shell.date2.split("_")

        # Casos abiertos por riesgo alto
        sqlquery1 = """select count(id) as 'Casos Criticos' from incident where
            date <= '{0}' and priority >= 8 and status not like '%Closed%';
        """.format(date2[0])

        # Casos abiertos por riesgo medio
        sqlquery2 = """select count(id) as 'Casos Medios' from incident where
            date <= '{0}' and priority between 5 and 7 and status
            not like '%Closed%';
        """.format(date2[0])

        # Casos abiertos por riesgo bajo
        sqlquery3 = """select count(id) as 'Casos Bajos' from incident where
            date <= '{0}' and priority <= 4 and
            status not like '%Closed%';
        """.format(date2[0])

        connect = mysql.connector.connect(
                user=args_shell.user,
                password=args_shell.password,
                host=args_shell.host,
                database=args_shell.database,
        )
        nivel_casos_abiertos = {}
        cursor = connect.cursor()
        cursor.execute(sqlquery1)
        for data in cursor:
            nivel_casos_abiertos['alto'] = data[0]

        cursor.execute(sqlquery2)
        for data in cursor:
            nivel_casos_abiertos['medio'] = data[0]

        cursor.execute(sqlquery3)
        for data in cursor:
            nivel_casos_abiertos['bajo'] = data[0]

        connect.close()
        return nivel_casos_abiertos

    def __casos_categoria(self):
        date1 = args_shell.date1.split("_")
        date2 = args_shell.date2.split("_")

        # Casos abiertos por categoria
        sqlquery1 = """select type_id as 'Categoria', count(id) as
            'Casos Abiertos' from incident where status not like '%Closed%'
            and date <='{0}' group by type_id;
        """.format(date2[0])

        # Casos cerrados por categoria
        sqlquery2 = """select a.type_id as 'Categoria', count(b.incident_id) as
            'Casos Cerrados' from incident a, incident_ticket b where b.status
            like '%Closed%' and b.date between '{0}' and '{1}'
            and a.id=b.incident_id group by a.type_id;
        """.format(date1[0], date2[0])

        connect = mysql.connector.connect(
                user=args_shell.user,
                password=args_shell.password,
                host=args_shell.host,
                database=args_shell.database,
        )
        nivel_casos_abiertos = list()
        cursor = connect.cursor()
        cursor.execute(sqlquery1)
        for data in cursor:
            nivel_casos_abiertos.append(data)

        nivel_casos_cerrados = list()
        cursor.execute(sqlquery2)
        for data2 in cursor:
            nivel_casos_cerrados.append(data2)

        # ordenamos la data en un diccionario convirtiendo los unicode en utf-8
        casos_abiertos_cat = dict()
        for data in nivel_casos_abiertos:
            casos_abiertos_cat[data[0].encode("utf-8")] = data[1]

        casos_cerrados_cat = dict()
        for data in nivel_casos_cerrados:
            casos_cerrados_cat[data[0].encode("utf-8")] = data[1]

        connect.close()

        info = {
            'abiertos': casos_abiertos_cat,
            'cerrados': casos_cerrados_cat
        }

        return info

    def get_data(self):
        data = {
             'casos_abiertos_cerrados': self.__casos(),
             'casos_riesgo': self.__casos_riesgo(),
             'alarmas_top': self.__top_alarma(),
             'vulnerabilidades': self.__vulnerabilidades(),
             'casos_categoria': self.__casos_categoria(),
        }

        return json.dumps(data)


if __name__ == '__main__':

    report = Reporte()
    print(report.get_data())
